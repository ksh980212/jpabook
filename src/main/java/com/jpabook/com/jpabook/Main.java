package com.jpabook.com.jpabook;

import com.jpabook.com.jpabook.entity.Member;
import com.jpabook.com.jpabook.entity.Order;
import com.jpabook.com.jpabook.entity.Product;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Main {

  public static void main(String[] args) {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpabook");
    EntityManager em = emf.createEntityManager();

    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();
      save(em);
      find(em);
      tx.commit();

    } catch (Exception e) {
      e.printStackTrace();
      tx.rollback();
    } finally {
      em.close();
    }

    emf.close();
  }
  private static void save(EntityManager em) {
    Member member1 = new Member();
    member1.setId("member1");
    member1.setName("회원1");
    em.persist(member1);

    Product productA = new Product();
    productA.setId("productA");
    productA.setName("상품1");
    em.persist(productA);

    Order order = new Order();
    order.setMember(member1);
    order.setProduct(productA);
    order.setOrderAmount(2);

    em.persist(order);
  }

  private static void find(EntityManager em) {
    Long orderId = 1L;
    Order order = em.find(Order.class, orderId);

    Member member = order.getMember();
    Product product = order.getProduct();

    System.out.println("member = " + member.getName());
    System.out.println("product = " + product.getName());
    System.out.println("orderAmount = " + order.getOrderAmount());
  }

}
