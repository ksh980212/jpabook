package com.jpabook.com.jpabook.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@Entity
public class Member {

  @Id
  @Column(name = "MEMBER_ID")
  private String id;

  private String name;

  @OneToMany(mappedBy = "member")
  private List<Order> memberProducts;
}
