package com.jpabook.com.jpabook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
public class Product {

  @Id
  @Column(name = "PRODUCT_ID")
  private String id;

  private String name;
  
}
